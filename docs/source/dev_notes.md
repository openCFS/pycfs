# Developer notes 

If you want to contribute to the pyCFS project, please take a look into our [contribution guidelines](dev_source/dev_notes_main.md).

```{toctree}
dev_source/dev_notes_main.md
dev_source/conventions.md
dev_source/data_handling.md
```