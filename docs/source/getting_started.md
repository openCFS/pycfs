# Getting started

:::{note}
Please make sure that you have set up your python environment as in the [installation](./installation.md#installation) section.
:::

To get you quickly up to speed please follow the [introductory examples](./examples/basics/introduction.md). 

For further features of the package please look over the contents or use the search bar to look for a specific topic.

```{toctree}
examples/basics/introduction.md
examples/basics/a_first_simulation.md
examples/basics/result_handling.md
examples/basics/cli_tool.md
examples/basics/data_processing.md
```