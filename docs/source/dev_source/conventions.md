## Conventions

### Directory and path variables 
Variables which denote a directory name are a `str` representing the name of the directory. If a variable represents a path to a certain directory than this variable must end with `'/'`.