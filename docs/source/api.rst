API Documentation
=================

Information on specific functions, classes, and methods.

.. toctree::
   :maxdepth: 2

   generated/pyCFS.data
   generated/pyCFS.pyCFS