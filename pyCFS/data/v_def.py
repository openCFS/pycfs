"""
Verbosity definitions

- ``all``:     all messages
- ``debug``:   debug messages
- ``more``:    more messages
- ``release``: release messages
- ``min``:     minimum messages
"""

all = 1000
debug = 500
more = 200
release = 100
min = 10
