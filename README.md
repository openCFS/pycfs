# pyCFS

Python library for automating and data handling tasks for openCFS. Available on [PyPI](https://pypi.org/project/pyCFS/).

For more information, please visit the [documentation page](https://opencfs.gitlab.io/pycfs/index.html).

### Included submodules:

- `pyCFS`: Automation library
- `pyCFS.data`: Data processing framework for CFS type hdf5 file format.
- `pyCFS.topt`: Topology Optimization with openCFS.
